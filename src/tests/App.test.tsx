import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { mount, configure } from "enzyme";

import App from "../App";

configure({ adapter: new Adapter() });

describe("Robot", () => {
  const sut = mount(<App />);

  [
    { command: "M", expecting: "Robot at (0, 1) facing North" },
    { command: "L", expecting: "Robot at (0, 1) facing West" },
    { command: "M", expecting: "Robot at (-1, 1) facing West" },
    { command: "M", expecting: "Robot at (-2, 1) facing West" },
    { command: "L", expecting: "Robot at (-2, 1) facing South" },
    { command: "R", expecting: "Robot at (-2, 1) facing West" },
    { command: "R", expecting: "Robot at (-2, 1) facing North" },
    { command: "R", expecting: "Robot at (-2, 1) facing East" },
    { command: "M", expecting: "Robot at (-1, 1) facing East" },
    {
      command: "?",
      expecting:
        "Command the robot with: L - turn left R - turn right M - move forward ? - this message Q - quit",
    },
    { command: "Q", expecting: "Robot shutting down." },
  ].forEach(({ command, expecting }) => {
    it(`by commanding the robot with ${command}, the output should be read: ${expecting}`, () => {
      sut
        .find(".command-input")
        .at(1)
        .simulate("change", { target: { value: command } });
      sut.find("Button").simulate("click");

      const directionText = sut.find(".output").text();
      expect(directionText).toEqual(expecting);
    });
  });
});
