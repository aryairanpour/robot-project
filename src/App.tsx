import React, { useEffect, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";

export const App: React.FC = () => {
  const [command, setCommand] = useState("");
  const [prompt, setPrompt] = useState<string>("");
  const [robotPosition, setRobotPosition] = useState<number[]>([0, 0]);
  const [robotDirection, setRobotDirection] = useState<string>("North");
  const [printInstructions, setPrintInstructions] = useState<boolean>(false);
  const [endProgram, setEndProgram] = useState<boolean>(false);

  const directions = ["North", "East", "South", "West"];

  const getNewDirection = (direction: string) => {
    let position;
    let newDirection;
    switch (direction) {
      case "L": {
        position = directions.indexOf(robotDirection) - 1;
        newDirection =
          position < 0
            ? directions[directions.length - 1]
            : directions[position];
        break;
      }
      case "R": {
        position = directions.indexOf(robotDirection) + 1;
        newDirection =
          position > directions.length - 1
            ? directions[0]
            : directions[position];
        break;
      }
      default:
        throw new Error();
    }
    return newDirection;
  };

  useEffect(() => {
    switch (command) {
      case "M": {
        setPrintInstructions(false);
        if (robotDirection === "North") {
          // Y axis + 1
          setRobotPosition([robotPosition[0], robotPosition[1] + 1]);
        } else if (robotDirection === "South") {
          // Y axis - 1
          setRobotPosition([robotPosition[0], robotPosition[1] - 1]);
        } else if (robotDirection === "West") {
          // X axis - 1
          setRobotPosition([robotPosition[0] - 1, robotPosition[1]]);
        } else {
          // X axis + 1
          setRobotPosition([robotPosition[0] + 1, robotPosition[1]]);
        }
        break;
      }
      case "L": {
        setPrintInstructions(false);
        const newDirection = getNewDirection(command);
        setRobotDirection(newDirection);
        break;
      }
      case "R": {
        setPrintInstructions(false);
        const newDirection = getNewDirection(command);
        setRobotDirection(newDirection);
        break;
      }
      case "?": {
        setPrintInstructions(true);
        break;
      }
      case "Q": {
        setEndProgram(true);
        break;
      }
      default:
        break;
    }
    setCommand("");
    setPrompt("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [command]);

  return (
    <Container className="my-5">
      {endProgram ? (
        <div className="output">Robot shutting down.</div>
      ) : (
        <>
          <div className="output">
            {printInstructions ? (
              <>
                Command the robot with:
                <ul>
                  <li> L - turn left</li>
                  <li> R - turn right</li>
                  <li> M - move forward</li>
                  <li> ? - this message</li>
                  <li> Q - quit</li>
                </ul>
              </>
            ) : (
              <p>
                Robot at ({robotPosition[0]}, {robotPosition[1]}) facing{" "}
                {robotDirection}
              </p>
            )}
          </div>
          <Form>
            <Row>
              <Col>
                <Form.Control
                  className="command-input"
                  placeholder="Input command"
                  value={prompt}
                  onChange={(e) => {
                    setPrompt(e.target.value);
                  }}
                />
              </Col>
              <Col>
                <Button
                  onClick={() => {
                    setCommand(prompt);
                  }}
                >
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>
        </>
      )}
    </Container>
  );
};

export default App;
